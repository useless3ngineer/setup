#!/bin/bash

#basic setup script 

# grab user parameters 
has_param() {
    local term="$1"
    shift
    for arg; do
        if [[ $arg == "$term" ]]; then
            return 0
        fi
    done
    return 1
}

setup_ram_hibernate() {


    if [[ $y == "NVIDIACorporation" ]]; then
    echo "Cannot setup Hibernate on this System with Nvidia Graphics"
    echo
    sleep 8
    elif [[ $answer == "y"  || $answer == "Y" ]]; then

        read -n 2 -p "Input size of swap in G up to 99:" swapsize
        swapoff -a
        fallocate -l $swapsize"G" /swapfile
        chmod 600 /swapfile
        mkswap /swapfile
        swapon /swapfile
        echo "/swapfile none swap defaults 0 0" | tee -a /etc/fstab
        v=$(filefrag -v /swapfile | awk '{ if($1=="0:"){print $4} }')

        v2=${v::-2}
        kernelstub -a "resume=/dev/mapper/data-root resume_offset=$v2"
        update-initramfs -u
        echo "HandleLidSwitch=suspend-then-hibernate" | tee -a /etc/systemd/logind.conf
        echo "HandleLidSwitchExternalPower=suspend-then-hibernate" | tee -a /etc/systemd/logind.conf
        echo "HibernateDelaySec=30min" | tee -a /etc/systemd/sleep.conf
    fi


}

desktop_setup() {
    sudo apt purge firefox gnome-2048 aisleriot atomix gnome-chess five-or-more hitori iagno gnome-klotski lightsoff gnome-mahjongg gnome-mines gnome-nibbles quadrapassel four-in-a-row gnome-robots gnome-sudoku swell-foop tali gnome-taquin gnome-tetravex -y & sudo apt autoremove -y
    apt install flatpak gnome-software-plugin curl make git vim htop wget
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak install flathub org.signal.Signal
    flatpak install flathub com.brave.Browser
    flatpak install flathub org.deluge_torrent.deluge
    flatpak install flathub com.vscodium.codium
    flatpak install flathub com.valvesoftware.Steam

    mkdir /home/$SUDO_USER/games
    echo ':set number' >> /home/$SUDO_USER/.vimrc
}

# check if user is running as root
if (( $EUID != 0 )); then
    echo "must be run as root"
    echo
    exit
fi
# install all if user uses flag -a
if has_param '-a' "$@"; then
    setup_ram_hibernate
    desktop_setup    
# install desktop tools
elif has_param '-d' "$@"; then
    desktop_setup
fi


